import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBasketShopping} from "@fortawesome/free-solid-svg-icons";

function BasketHeader(props) {
    return (
        <div className="basket-header d-flex justify-content-center align-items-center">
            <FontAwesomeIcon  size="lg" icon={faBasketShopping} />
            <h2>Basket</h2>
        </div>
    );
}

export default BasketHeader;