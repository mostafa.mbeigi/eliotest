import React from 'react';

function Pizza({pizza}) {
    return (
        <div className="pizza">
            <div className="d-flex justify-content-between">
                <div>{pizza.name}</div>
                <div>{pizza.price} &euro;</div>
            </div>
            <div className="pizza-description">{pizza.description}</div>
        </div>
    );
}

export default Pizza;