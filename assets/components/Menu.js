import React from 'react';
import {Link} from "react-router-dom";

function Menu({navigation}) {
    return (
        <ul className="menu d-flex justify-content-between align-items-center">
            {
                navigation.map(nav=>{
                    return <div key={nav["@id"]}>
                        <Link to={nav.path}>{nav.title}</Link>
                    </div>
                })
            }
        </ul>
    );
}

export default Menu;