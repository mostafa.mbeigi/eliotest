import React, {useContext, useState} from 'react';
import Basket from "./Basket";
import {Link} from "react-router-dom";
import {basketContext} from "./App";
import instance from "../interceptor";
import UserFormData from "./UserFormData";
function Checkout(props) {
    const {basket,setBasket,loadingBasket} = useContext(basketContext)
    function registerOrder(formData){
        registerOrderDb(formData).then(res=>{
            destroyCart()
        }).catch(err=>{
        })
    }
    function registerOrderDb(formData){
        return instance({
            method:"post",
            url:"/orders",
            data:{
                "data":{
                    ...formData
                }
            }
        })
    }
   function destroyCart(){
        destroyDb().then(res=>{
            setBasket({cart_total:0,total_items:0})
        }).catch(err=>{

        })
    }
    function destroyDb(){
        return instance({
            method:"delete",
            url:"/carts/all"
        })
    }
    return (
       !loadingBasket &&
        <div className="col-lg-12">
            <Basket/>
            {
                basket.total_items===0 ?
                    <h2>Your Cart is empty </h2>
                :
                    <div>
                        <UserFormData handleSubmitForm={registerOrder}/>
                    </div>
            }
         {/*  <button onClick={()=>destroy()}>delete</button>*/}
        </div>
    );
}

export default Checkout;