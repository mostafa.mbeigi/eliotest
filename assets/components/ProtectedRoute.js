import React, { useEffect, useState, useContext } from "react";
import {Navigate} from "react-router";
import {authContext} from "./App";

function ProtectedRoute({ component: Component, ...restOfProps }) {
    const { auth } = useContext(authContext);
    if(!auth.loading){
        if (!auth.auth) {
            return <Navigate to="/not-authorized" replace />;
        }
        return <Component />;
    }

}

export default ProtectedRoute;
