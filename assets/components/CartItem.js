import React, {useContext, useState} from 'react';
import Qty from "./Qty";
import PizzaInCart from "./PizzaInCart";
import instance from "../interceptor";
import {basketContext} from "./App";
import {faClose, faSpinner, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

function CartItem({item}) {
    const {setBasket} = useContext(basketContext)
    const [loading,setLoading] = useState(false)
    function removeFromCart(){
        setLoading(true)
        removeFromCartDb().then(res=>{
            setBasket(res.data.data)
        }).catch(err=>{

        })
    }
    function handleChangeQty(qty){
        changeItemQtyDb(qty).then(res=>{
            setBasket(res.data.data)
        }).catch(err=>{

        })
    }
    const changeItemQtyDb = (qty)=>{
        return instance({
            method : "put",
            url: `/carts/${item[0]}`,
            data : {
                data:{
                    qty : qty
                }
            }
        })
    }
    const removeFromCartDb = ()=>{
        return instance({
            method : "delete",
            url: `/carts/${item[0]}`,
        })
    }
    return (
        <>
            {
                loading ?
                         <FontAwesomeIcon icon={faSpinner} pulse />
                    :    <FontAwesomeIcon className="pointer trash" size="lg" onClick={()=>removeFromCart()} icon={faTrash} />
            }

            <div className="d-flex justify-content-between">
                <div>qty</div>
                <Qty itemId={item[0]} handleChangeQty={handleChangeQty} defaultQty={item[1].qty}/>
            </div>
            <PizzaInCart rowId={item[0]} pizza={item[1]}/>
            <div className="d-flex justify-content-between">
                <div>Total</div>
                {parseFloat(item[1].subtotal).toFixed(2)} &euro;
            </div>
            {
                item[1].discount &&
                <div className="d-flex justify-content-between">
                    <div>Your benefit</div>
                    {parseFloat(item[1].discount).toFixed(2)} &euro;
                </div>
            }
        </>
    );
}

export default CartItem;