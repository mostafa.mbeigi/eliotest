import React,{useEffect,useState,useContext} from 'react';
import {Link} from "react-router-dom";
import SwitchLanguage from "./SwitchLanguage";
import instance from "../interceptor";
import {languageContext} from "./App";
import MiniBasket from "./MiniBasket";
import Menu from "./Menu";
import Breadcrumb from "./Breadcrumb";

function Header() {
    const {language} = useContext(languageContext)
    const [navigation,setNavigation] = useState([])
    useEffect(()=>{
        fetchNavigation().then(res=>{
            setNavigation(res.data["hydra:member"])
        }).catch(err=>{

        })
    },[language])
    const fetchNavigation = ()=>{
       return  instance.get("/navigations", {
            headers: {
                'Accept-Language': language
            }
        })
    }
    return (
        <header>
            <div className="row header align-items-center">
                <div className="col-lg-5">
                    <Menu navigation={navigation}/>
                </div>
                <div className="col-lg-1 offset-lg-5">
                    <SwitchLanguage/>
                </div>
                <div className="col-lg-1">
                    <MiniBasket/>
                </div>
            </div>
            <div className="row">
                <Breadcrumb navigation={navigation}  />
            </div>
        </header>


    );
}

export default Header;