import React,{useEffect,useState} from 'react';
import instance from "../interceptor";
import Pizza from "./Pizza";
import AddToCart from "./AddToCart";
import Basket from "./Basket";
import ExtraModal from "./ExtraModal";
import Qty from "./Qty";

function OnlineOrder(props) {
    const [pizzas,setPizzas] = useState([]);
    const [qty,setQty] = useState(1);
    useEffect(()=>{
        fetchPizzas().then(res=>{
            setPizzas(res.data["hydra:member"])
        }).catch(err=>{

        })
    },[])
    const fetchPizzas = ()=>{
        return instance({
            method: "get",
            url: "/pizzas"
        })
    }
    function handleChangeQty(qty){
        setQty(qty)
    }
    return (
        <div className="col-lg-12 mt-4">
            {
                pizzas.map(pizza=>{
                   return <div className="row each-pizza-list" key={pizza["@id"]}>
                       <div className="col-lg-10">
                           <Pizza pizza={pizza}/>
                       </div>
                       <div className="col-lg-2">
                           <div className="d-flex justify-content-between align-items-center">
                               <Qty handleChangeQty={handleChangeQty} />
                               <AddToCart qty={qty} pizza={pizza}/>
                           </div>

                       </div>

                   </div>
                })
            }
        </div>
    );
}

export default OnlineOrder;