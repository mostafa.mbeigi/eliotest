import React from 'react';

function Extra({extra}) {
    return (
        <div>
            {extra.name}
            {extra.price} &euro;
        </div>
    );
}

export default Extra;