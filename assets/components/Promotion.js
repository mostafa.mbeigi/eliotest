import React, {useEffect, useState} from 'react';
import instance from "../interceptor";
import Pizza from "./Pizza";

function Promotion(props) {
    const [promotions,setPromotions] = useState([])
    useEffect(()=>{
        getPromotionsDb().then(res=>{
            setPromotions(res.data.data)
        }).catch(err=>{

        })
    },[])
    const getPromotionsDb = ()=>{
        return instance({
            method : "get",
            url :"/promotions"
        })
    }
    return (
        <div>
            <h2>Pizza of the Day</h2>
            {
                promotions.map(promotion=>{
                    return <div key={promotion.id}>
                        <Pizza pizza={promotion.pizza}/>
                    </div>
                })
            }
        </div>
    );
}

export default Promotion;