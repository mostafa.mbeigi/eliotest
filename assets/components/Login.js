import React, {useContext, useState} from 'react';
import {useForm} from "react-hook-form";
import {Button, Spinner} from "react-bootstrap";
import instance from "../interceptor";
import {authContext} from "./App";
import {Navigate,useNavigate} from "react-router-dom";

function Login(props) {
    const { auth,setAuth } = useContext(authContext);
    const [loading,setLoading] = useState(false)
    let navigate = useNavigate();
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    const onSubmit = (data) => {
        setLoading(true)
        checkAuth(data.username,data.password).then(res=>{
            setLoading(false)
            if(res.status === 200){
                localStorage.setItem('token',res.data.token)
                setAuth({loading:false,auth:true})
                return navigate("/dashboard");
            }
        }).catch(err=>{
            setLoading(false)
        })
    }
    const checkAuth = (username,password)=>{
        return instance({
            method:"post",
            url:"/login_check",
            data:{
                username:username,
                password:password
            }
        })
    }
    const asyncLocalStorage = {
        setItem: function (key, value) {
            return Promise.resolve().then(function () {
                localStorage.setItem(key, value);
            });
        },
        getItem: function (key) {
            return Promise.resolve().then(function () {
                return localStorage.getItem(key);
            });
        }
    };
    if(!auth.loading){
        if(!auth.auth){
            return (
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-group">
                        <label htmlFor="lastname">Username</label>
                        <input {...register("username", { required: "This field is required" })} type="text" className="form-control" placeholder="Enter Username" />
                        {errors.username && (
                            <div className="alert alert-danger">{errors.username.message}</div>
                        )}
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastname">Password</label>
                        <input {...register("password", { required: "This field is required" })} type="password" className="form-control" placeholder="Enter Password" />
                        {errors.password && (
                            <div className="alert alert-danger">{errors.password.message}</div>
                        )}
                    </div>
                    <Button variant="primary" type="submit" disabled={loading}>
                        {loading
                            ? [
                                <Spinner
                                    key="0"
                                    as="span"
                                    animation="grow"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                />,
                                "Loading...",
                            ]
                            : `Login`}
                    </Button>
                </form>
            );
        }
        else{
           return <Navigate to="/dashboard" />
        }
    }

}

export default Login;