import React,{useContext} from 'react';
import BasketTotal from "./BasketTotal";
import BasketHeader from "./BasketHeader";
import BasketItems from "./BasketItems";
import {Link} from "react-router-dom";
import {basketContext} from "./App";
import RemoveCart from "./RemoveCart";

function Basket(props) {
    const {basket} = useContext(basketContext)
    return (
        <div className="basket col-lg-12">
            {
                basket.total_items>0 &&
                    <>
                        <BasketItems/>
                        <div className="p-2">
                            <BasketTotal/>
                        </div>
                        <RemoveCart />
                    </>
            }

        </div>
    );
}

export default Basket;