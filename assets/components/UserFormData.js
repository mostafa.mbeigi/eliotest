import React, {useContext, useEffect, useRef, useState} from 'react';
import {basketContext} from "./App";
import {useForm} from "react-hook-form";
import instance from "../interceptor";
import { Spinner, Button } from "react-bootstrap";

function UserFormData({handleSubmitForm,loading}) {
    const {basket,setBasket} = useContext(basketContext);
    let [delivery,setDelivery] = useState( ("delivery_fee" in basket))
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    useEffect(()=>{
           setDelivery(("delivery_fee" in basket))
    },[basket])
    const onSubmit = (data) => {
        handleSubmitForm(data)
    }
    function changeDelivery(value){
        setDelivery(value)
        updateDeliveryDb(value).then(res=>{
            setBasket(res.data.data)
        })
    }
    const updateDeliveryDb = (value)=>{
        return instance({
            method:"put",
            url:"/carts",
            data:{
                "data":{
                    delivery: value
                }
            }
        })
    }
    return (
        <div>
            {
                basket.cart_total>10 &&
                <div className="form-check">
                    <input onChange={(e)=>changeDelivery(e.target.checked)} type="checkbox" defaultChecked={delivery} name="delivery" className="form-check-input"  />
                    <label className="form-check-label" htmlFor="exampleCheck1">Deliver to my address</label>
                </div>
            }
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group">
                    <label htmlFor="firstName">Firstname</label>
                    <input {...register("firstname", { required: "This field is required" })} type="text" className="form-control" placeholder="Enter First name" />
                    {errors.firstname && (
                        <div className="alert alert-danger">{errors.firstname.message}</div>
                    )}
                </div>
                <div className="form-group">
                    <label htmlFor="lastname">Lastname</label>
                    <input {...register("lastname", { required: "This field is required" })} type="text" className="form-control" placeholder="Enter Last name" />
                    {errors.lastname && (
                        <div className="alert alert-danger">{errors.lastname.message}</div>
                    )}
                </div>
                <div className="form-group">
                    <label htmlFor="street">Street</label>
                    <input {...register("street", { required: delivery && "This field is required" })} type="text" className="form-control" placeholder="Enter Street" />
                    {errors.street && (
                        <div className="alert alert-danger">{errors.street.message}</div>
                    )}
                </div>
                <div className="form-group">
                    <label htmlFor="streetnumber">streetnumber</label>
                    <input {...register("streetnumber", { required: delivery && "This field is required" })} type="text" className="form-control" placeholder="Enter Streetnumber" />
                    {errors.streetnumber && (
                        <div className="alert alert-danger">{errors.streetnumber.message}</div>
                    )}
                </div>
                <div className="form-group">
                    <label htmlFor="zip">Zip</label>
                    <input {...register("zip", { required: delivery && "This field is required" })} type="text" className="form-control" placeholder="Enter Zip" />
                    {errors.zip && (
                        <div className="alert alert-danger">{errors.zip.message}</div>
                    )}
                </div>
                <div className="form-group">
                    <label htmlFor="city">City</label>
                    <input {...register("city", { required: delivery && "This field is required" })} type="text" className="form-control" placeholder="Enter City" />
                    {errors.city && (
                        <div className="alert alert-danger">{errors.city.message}</div>
                    )}
                </div>
                <div className="form-group">
                    <label htmlFor="phone">Phone</label>
                    <input {...register("phone")} type="phone" className="form-control" placeholder="Enter Phone" />
                </div>
                <Button variant="primary" type="submit" disabled={loading}>
                    {loading
                        ? [
                            <Spinner
                                key="0"
                                as="span"
                                animation="grow"
                                size="sm"
                                role="status"
                                aria-hidden="true"
                            />,
                            "Loading...",
                        ]
                        : ` Checkout`}
                </Button>
            </form>
        </div>
    );
}

export default UserFormData;