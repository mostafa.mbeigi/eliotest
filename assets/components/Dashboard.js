import React, {useEffect, useState} from 'react';
import AuthInstance from "../authInterceptor";
import OrderItems from "./OrderItems";
import Customer from "./Customer";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faSpinner, faTrash} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";

function Dashboard(props) {

    return (
        <div>
           <h3><Link to="/orders"> Orders List</Link></h3>
        </div>
    );
}

export default Dashboard;