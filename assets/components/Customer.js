import React from 'react';

function Customer({customer}) {
    return (
        <div className="d-flex customer">
            <div>firstname : {customer.firstname}</div>
            <div>lastname : {customer.lastname}</div>
        </div>
    );
}

export default Customer;