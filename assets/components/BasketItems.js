import React, {useContext} from 'react';
import {basketContext} from "./App";
import CartItem from "./CartItem";

function BasketItems() {
    const {basket} = useContext(basketContext)
    return (

            Object.entries(basket).map((item,index) => {
                if(item[0]!== "cart_total" && item[0]!== "total_items" && item[0]!== "delivery_fee" && item[0]!== "cart_subtotal"){
                    return  <div className="p-2 basket-item" key={item[0]}>
                              <CartItem item={item}/>
                           </div>
                }
            })


    );
}

export default BasketItems;