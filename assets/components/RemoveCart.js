import React, {useContext, useState} from 'react';
import {Button, Spinner} from "react-bootstrap";
import instance from "../interceptor";
import {basketContext} from "./App";

function RemoveCart(props) {
    const [loading,setLoading] = useState(false)
    const {setBasket} = useContext(basketContext)
    function removeCart(){
        setLoading(true)
        removeCartDb().then(res=>{
            setBasket({cart_total: 0,total_items:0})
            setLoading(false)
        }).catch(err=>{

        })
    }
    const removeCartDb = ()=>{
        return instance({
            method : "delete",
            url : `/carts/all`
        })
    }
    return (
        <Button onClick={()=>removeCart()} variant="danger" type="submit" disabled={loading}>
            {loading
                ? [
                    <Spinner
                        key="0"
                        as="span"
                        animation="grow"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                    />,
                    "Loading...",
                ]
                : ` Remove all Items`}
        </Button>
    );
}

export default RemoveCart;