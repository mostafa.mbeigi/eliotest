import React,{useEffect,useState} from 'react';
import { Routes, Route } from "react-router-dom"
import Home from "./Home"
import About from "./About"
import Header from "./Header";
import OnlineOrder from "./OnlineOrder";
import instance from "../interceptor";
import Cookie from "./Cookie";
import Checkout from "./Checkout";
import ProtectedRoute from "./ProtectedRoute";
import Dashboard from "./Dashboard";
import Login from "./Login";
import AuthInstance from "../authInterceptor";
import Orders from "./Orders";
export const languageContext = React.createContext("en")
export const basketContext = React.createContext({})
export const authContext = React.createContext({})
function App(props) {
    const [auth,setAuth] = useState({loading:true,auth:false})
    const [language,setLanguage] = useState("en")
    const [loadingBasket,setLoadingBasket] = useState(true)
    const [basket,setBasket] = useState({})
    useEffect(()=>{
        fetchBasketDb().then(res=>{
            setBasket(res.data.data)
            setLoadingBasket(false)
        }).catch(err=>{
        })
    },[])
    useEffect(() => {
        checkUserAuth().then(res=>{
            setAuth({loading:false,auth:res.data.status})
        }).catch(err=>{
            setAuth({loading:false,auth:false})
        })
    }, []);
    const fetchBasketDb = ()=>{
        return instance({
            method:"get",
            url:"/carts"
        })
    }
    function checkUserAuth(){
        return AuthInstance({
            method:"get",
            url:"/users/me"
        })
    }
    return (
        !loadingBasket && !auth.loading &&
        <div className="App">
            <authContext.Provider value={{auth,setAuth}}>
                <languageContext.Provider value={{language,setLanguage}}>
                    <basketContext.Provider value={{basket,setBasket,loadingBasket}}>
                        <div className="container-fluid gray">
                            <div className="container">
                                <Header/>
                            </div>
                        </div>
                        <div className="container">
                            <Routes>
                                <Route path="/" element={ <Home/> } />
                                <Route path="/about" element={ <About/> } />
                                <Route path="/online-order" element={ <OnlineOrder/> } />
                                <Route path="/checkout" element={ <Checkout/> } />
                                <Route path="/login" element={ <Login/> } />
                                <Route
                                    path="dashboard"
                                    element={
                                        <ProtectedRoute component={Dashboard}>
                                            <Dashboard />
                                        </ProtectedRoute>
                                    }
                                />
                                <Route
                                    path="orders"
                                    element={
                                        <ProtectedRoute component={Orders}>
                                            <Orders />
                                        </ProtectedRoute>
                                    }
                                />
                            </Routes>
                        </div>
                    </basketContext.Provider>
                </languageContext.Provider>
            </authContext.Provider>

            <Cookie/>
        </div>
    );
}

export default App;