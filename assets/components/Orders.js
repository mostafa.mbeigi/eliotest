import React, {useEffect, useState} from 'react';
import AuthInstance from "../authInterceptor";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faSpinner} from "@fortawesome/free-solid-svg-icons";
import Customer from "./Customer";
import OrderItems from "./OrderItems";

function Orders(props) {
    const [orders,setOrders] = useState([])
    const [loading,setLoading] = useState(false)
    useEffect(()=>{
        fetchOrders().then(res=>{
            setOrders(res.data.data)
            console.log(res.data.data)
        })
    },[])
    function changeOrderStatus(id){
        setLoading(true)
        changeOrderStatusDb(id).then(res=>{
            setLoading(false)
            setOrders((prevState) =>
                prevState.filter((item) => {
                    return   item.id !== id
                })
            );
        }).catch(err=>{

        })
    }
    const fetchOrders = ()=>{
        return AuthInstance({
            method:"get",
            url:"/orders",
            params:{
                "status":"received"
            }
        })
    }
    const changeOrderStatusDb = (id)=>{
        return AuthInstance({
            method:"patch",
            url:`/orders/${id}`,
            data:{
                "data":{
                    status: "processed"
                }
            }
        })
    }
    return (
        <div>
            {
                orders.map(order=>{
                    return <div className="each-order" key={order.id}>
                        {
                            loading ?
                                <FontAwesomeIcon icon={faSpinner} pulse />
                                :    <FontAwesomeIcon className="pointer trash" size="lg" onClick={()=>changeOrderStatus(order.id)} icon={faCheck} />
                        }
                        <Customer customer={order.customer}/>
                        <OrderItems orderItems={order.orderItems}/>
                        <div>Total : {order.total_price}</div>
                    </div>
                })
            }
        </div>
    );
}

export default Orders;