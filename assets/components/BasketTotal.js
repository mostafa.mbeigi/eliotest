import React, {useContext} from 'react';
import {basketContext} from "./App";
import {Link} from "react-router-dom";

function BasketTotal() {
    const {basket} = useContext(basketContext)
    return (
        <div className="d-flex flex-column">
            <div className="d-flex justify-content-between">
                <div>Subtotal</div>
                <div>{parseFloat(basket.cart_subtotal).toFixed(2)} &euro;</div>
            </div>
            {
                "delivery_fee" in basket && basket.delivery_fee>0 &&
                <div className="d-flex justify-content-between">
                    <div>Delivery fee</div>
                    <div>{parseFloat(basket.delivery_fee).toFixed(2)} &euro;</div>
                </div>
            }
            <div className="d-flex justify-content-between">
                <div>Total</div>
                <div>{parseFloat(basket.cart_total).toFixed(2)} &euro;</div>
            </div>
        </div>
    );
}

export default BasketTotal;