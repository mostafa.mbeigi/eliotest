import React from 'react';
import PizzaInCart from "./PizzaInCart";
import OrderItem from "./OrderItem";

function OrderItems({orderItems}) {
   return (
        orderItems.map(item=>{
            return <div key={item.id}>
                        <OrderItem orderItem={item}/>
                   </div>
        })
    );
}

export default OrderItems;