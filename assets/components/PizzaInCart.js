import React from 'react';
import Extra from "./Extra";
import Pizza from "./Pizza";
import RemoveExtra from "./RemoveExtra";

function PizzaInCart({pizza,rowId}) {
    return (
        <div className="d-flex flex-column pizza-in-cart">
            <div className="d-flex justify-content-between align-items-center">
                <Pizza pizza={pizza}/>
            </div>
            <div className="extras-in-cart">
                {
                    pizza.extras &&
                        pizza.extras.map(extra=>{
                          return   <div key={extra.id}>
                              <div className="d-flex">
                                  <RemoveExtra extraId={extra.id} rowId={rowId} />
                                  <Extra  extra={extra}/>
                              </div>

                            </div>

                        })
                }
            </div>
        </div>
    );
}

export default PizzaInCart;