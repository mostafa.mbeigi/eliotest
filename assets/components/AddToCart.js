import React, {useContext, useState} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPlus, faSpinner} from '@fortawesome/free-solid-svg-icons'
import ExtraModal from "./ExtraModal";
import instance from "../interceptor";
import {basketContext} from "./App";
function AddToCart({pizza,qty}) {
    const {setBasket} = useContext(basketContext)
    const [loading,setLoading] = useState(false)
    const [showExtraModal,setShowExtraModal] = useState(false)
    const [extras,setExtras] = useState([])
    function handleSaveItem(extras){
        saveItemDb(extras).then(res=>{
            setBasket(res.data.data)
        }).catch(err=>{

        })
    }
    function addCart(){
        setLoading(true)
        fetchPizzaExtrasDb().then(res=>{
            setLoading(false)
            if(res.data["hydra:member"].length>0){
                setShowExtraModal(true)
                setExtras(res.data["hydra:member"])
                return;
            }
            handleSaveItem(null);
        }).catch(err=>{

        })
    }
    const fetchPizzaExtrasDb = ()=>{
        return instance({
            method:"get",
            url:`/pizzas/${pizza.id}/extras`
        })
    }
    const saveItemDb = (extras)=>{
        const data = extras ? { pizzas:pizza.id,extras:extras,qty:qty } : { pizzas:pizza.id,qty:qty }
        return instance({
            method:"post",
            url:`/carts`,
            data: {
                "data":data
            }
        })
    }
    return (
        <div className="add-icon">
            {
                loading ?
                    <FontAwesomeIcon icon={faSpinner} pulse />
                :   <FontAwesomeIcon size="lg" onClick={()=>addCart()} icon={faPlus} />
            }
            <ExtraModal setShow={setShowExtraModal} show={showExtraModal} pizza={pizza} extras={extras} handleSaveItem={handleSaveItem}/>
        </div>
    );
}

export default AddToCart;