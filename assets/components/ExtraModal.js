import React,{useState} from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import ExtraCheckbox from "./ExtraCheckbox";
import useCalculateItemPrice from "../hooks/useCalculateItemPrice";
function ExtraModal({show,setShow,extras,handleSaveItem,pizza}) {
    const [checkedExtras,setCheckedExtras] = useState([])
    const itemTotalPrice = useCalculateItemPrice(pizza,checkedExtras)
    const handleChangeCheckbox = (checked,extraChecked)=>{
       checked
           ?
           setCheckedExtras(prevState => [...prevState,extraChecked])
           :
            setCheckedExtras(prevState=>
                prevState.filter((item) => {
                   return  item.id !== extraChecked.id
                })
            );
    }
    function addToCart(){
        setShow(false)
        let extrasIds = []
         extrasIds = checkedExtras.map(res=>  res.id)
         handleSaveItem(extrasIds)
    }
    const handleClose = () => setShow(false);
    return (
        <div>
                <Modal show={show} onHide={handleClose}  animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>{pizza.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {
                            extras.map(extra=>{
                                return <div key={extra.id}>
                                    <ExtraCheckbox extra={extra} handleChangeCheckbox={handleChangeCheckbox}/>
                                </div>
                            })
                        }
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={()=>addToCart()} className="d-flex justify-content-center w-100" variant="primary">
                            Add
                        </Button>
                    </Modal.Footer>
                </Modal>
        </div>
    );
}

export default ExtraModal;