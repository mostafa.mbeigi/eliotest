import React, {useEffect, useState} from 'react';
function Qty({defaultQty,handleChangeQty,itemId}) {
 const [qty,setQty] = useState(defaultQty)
    useEffect(()=>{
        setQty(defaultQty)
    },[defaultQty])
    function changeQty(qty){
        setQty(qty)
        handleChangeQty(qty,itemId)
    }
    return (
        <div>
            <select value={qty} className="form-select" onChange={(e)=>changeQty(e.target.value)}  name="qty" id="">
                {(() => {
                    let options = []
                    for (let i = 1; i < 15; i++) {
                        options.push(<option key={i} value={i}>{i}</option>);
                    }
                    return options;
                })()}
            </select>
        </div>
    );
}

export default Qty;