import React,{useContext} from 'react';
import {languageContext} from "./App"
import german from "../images/german.png"
import british from "../images/british.png"
function SwitchLanguage(props) {
    const {language,setLanguage} = useContext(languageContext)
    function switchLanguage(lang){
        setLanguage(lang)
        document.documentElement.lang = lang;
    }
    return (
               <div className="switch-lang">
                   {
                       language==="en" ?
                           <img src={german} onClick={()=>switchLanguage("de")} />
                       :    <img src={british} onClick={()=>switchLanguage("en")} />
                   }
               </div>
    );
}

export default SwitchLanguage;