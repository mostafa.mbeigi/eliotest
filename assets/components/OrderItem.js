import React from 'react';
import Pizza from "./Pizza";
import Extra from "./Extra";

function OrderItem({orderItem}) {
    return (
        <div className="order-item">
            <Pizza pizza={orderItem.pizzas}/>
            <div>Quantity : {orderItem.quantity}</div>
            {
                orderItem.extras.map(extra=>{
                    return <div key={extra.id}>
                        <Extra extra={extra}/>
                    </div>
                })
            }
        </div>
    );
}

export default OrderItem;