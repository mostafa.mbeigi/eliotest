import React from 'react';

function ExtraCheckbox({extra,handleChangeCheckbox}) {
    return (
        <div className="form-check">
            <input onChange={(e)=>handleChangeCheckbox(e.target.checked,extra)} className="form-check-input" type="checkbox" value="" id="defaultCheck1" />
                <label className="form-check-label" htmlFor="defaultCheck1">
                    {extra.name}
                </label>
            <span>({extra.price})</span>
        </div>
    );
}

export default ExtraCheckbox;