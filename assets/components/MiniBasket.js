import React,{useContext} from 'react';
import {basketContext} from "./App";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBasketShopping} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";

function MiniBasket(props) {
    const {basket} = useContext(basketContext)
    return (
        <Link to="/checkout">
            <div className="d-flex">
                <FontAwesomeIcon  icon={faBasketShopping} />
                <div>{basket.total_items}</div>
            </div>
            <div>
                {parseFloat(basket.cart_total).toFixed(2)}
            </div>
        </Link>
    );
}

export default MiniBasket;