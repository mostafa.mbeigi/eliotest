import React from 'react';
import {Link, useLocation} from 'react-router-dom'
function Breadcrumb({navigation}) {
    const location = useLocation();
    return (
        <div className="col-lg-12">
            <nav>
                <ol className="breadcrumb">
                    {
                        location.pathname === "/" ?
                            <li className="breadcrumb-item">Pizza Plaza</li>
                            :   <li className="breadcrumb-item"><Link to="/">Pizza Plaza</Link></li>
                    }
                    {
                        navigation.map(nav=>{
                            if(nav.path===location.pathname && location.pathname!=="/"){
                                return <li className="breadcrumb-item" key={nav.id}>
                                    <span>{nav.title}</span>
                                </li>
                            }

                        })
                    }


                </ol>
            </nav>
        </div>

    );
}

export default Breadcrumb;