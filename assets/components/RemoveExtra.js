import React, {useContext, useState} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck, faSpinner, faTrash} from "@fortawesome/free-solid-svg-icons";
import {basketContext} from "./App";
import instance from "../interceptor";

function RemoveExtra({extraId,rowId}) {
    const [loading,setLoading] = useState(false)
    const {setBasket} = useContext(basketContext)
    function removeExtra(){
        setLoading(true)
        removeExtraDb().then(res=>{
            setBasket(res.data.data)
            setLoading(false)
        }).catch(err=>{

        })
    }
    const removeExtraDb = ()=>{
        return instance({
            method : "delete",
            url : `carts/${rowId}/extras/${extraId}`
        })
    }
    return (
        <div>
            {
                loading ?
                    <FontAwesomeIcon icon={faSpinner} pulse />
                    :    <FontAwesomeIcon className="pointer trash" onClick={()=>removeExtra()} icon={faTrash} />
            }
        </div>
    );
}

export default RemoveExtra;