import React, {useEffect} from 'react';
const cookieTitle = "Cookies.";
const cookieDesc = "By using this website, you automatically accept that we use cookies.";
const cookieButton = "Consent";
const cookie_show_again_in_day = 180;
function Cookie(props) {
    useEffect(()=>{
            let timeToShowCookie = setNewDate(-1);
            if(localStorage.getItem('cookie_show')){
                timeToShowCookie = new Date(parseInt(localStorage.getItem('cookie_show')));
            }
            if (new Date() > timeToShowCookie) {
                fadeIn("cookieConsentContainer");
            }

    },[])
    function setNewDate(days){
        let date = new Date();
        return date.getTime() + (days*24*60*60*1000);
    }
    function setCookie(name,value,cookie_validity) {
        let expires = "";
        if (cookie_validity) {
            let date = new Date();
            date.setTime(cookie_validity);
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
    function fadeIn(elem, display){
        let el = document.getElementById(elem);
        el.style.opacity = 0;
        el.style.display = display || "block";
        (function fade() {
            let val = parseFloat(el.style.opacity);
            if (!((val += .02) > 1)) {
                el.style.opacity = val;
                requestAnimationFrame(fade);
            }
        })();
    }
    function fadeOut(elem){
        let el = document.getElementById(elem);
        el.style.opacity = 1;
        (function fade() {
            if ((el.style.opacity -= .02) < 0) {
                el.style.display = "none";
            } else {
                requestAnimationFrame(fade);
            }
        })();
    }
    function cookieAgree() {
        let future = setNewDate(cookie_show_again_in_day)
        setCookie('elioCookie','1',future);
        localStorage.setItem('cookie_show',future)
        fadeOut("cookieConsentContainer");
    }
    return (
       <div className="cookieConsentContainer" id="cookieConsentContainer">
           <div className="cookieTitle">
               <a>{cookieTitle}</a>
           </div>
           <div className="cookieDesc">
           <p>
               {cookieDesc }
               <a href="" target="_blank">What for?</a>
           </p>
       </div>
           <div className="cookieButton">
              <a onClick={()=>cookieAgree()}>{cookieButton}</a>
           </div>
       </div>

);
}

export default Cookie;