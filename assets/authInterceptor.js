
import axios from "axios";
const AuthInstance = axios.create({
    baseURL: `http://104.248.31.198/api/v1`,
    headers: {
        Accepted: 'appication/json',
        'Content-Type': 'application/json',
    },
});

AuthInstance.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem('token');
        if (token) {
            config.headers.authorization = token;
        }
        return config;
    },
    (error) => Promise.reject(error),
);

export default AuthInstance;