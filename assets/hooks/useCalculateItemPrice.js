import React, {useEffect, useState} from 'react';

function useCalculateItemPrice(pizza,extras) {
    const [totalPrice,setTotalPrice] = useState(0)
    useEffect(()=>{
        calculateExtras().then(extraPrice=>{
            setTotalPrice(extraPrice + parseFloat(pizza.price))
        })

    },[pizza,extras])
    async function calculateExtras(){
        let extraPrice = 0;
        await  extras.map(extra=>{
            extraPrice += parseFloat(extra.price)
        })
        return extraPrice;
    }
    return parseFloat(totalPrice).toFixed(2);
}

export default useCalculateItemPrice;