<?php


namespace App\Classes;


use App\Entity\Promotion;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\SerializerInterface;

class Shop extends AbstractController
{
    protected const AMOUNT_FOR_ACTIVATION_DELIVERY= 10;
    protected const DELIVERY_FLAT_RATE= 25;
    protected const DELIVERY_FEE = 1.5;
    protected $cart_contents = array();
    protected $em;
    protected $serializer;
    protected $session;
    private $dispatcher;
    protected $delivery = false;
    protected $pizzasPromoted;

    public function __construct(EntityManagerInterface $em,SerializerInterface $serializer,SessionInterface $session,EventDispatcherInterface $dispatcher){
        $this->session = $session;
        $this->em = $em;
        $this->serializer = $serializer;
        $this->dispatcher = $dispatcher;
        $this->cart_contents = !empty($this->session->get('cart_contents'))?$this->session->get('cart_contents'):NULL;
        if ($this->cart_contents === NULL){
            $this->cart_contents = array('cart_total' => 0, 'total_items' => 0);
        }
        if(array_key_exists('delivery_fee', $this->cart_contents)){
            $this->delivery = true;
        }
        $this->pizzasPromoted = $this->em->getRepository(Promotion::class)->findAll();
    }


    public function calculate_delivery_fee(): void
    {
        if($this->delivery && $this->cart_contents['cart_total'] > self::AMOUNT_FOR_ACTIVATION_DELIVERY){
            $this->cart_contents['delivery_fee'] = self::DELIVERY_FEE;
            if($this->cart_contents['cart_total'] > self::DELIVERY_FLAT_RATE){
                $this->cart_contents['delivery_fee'] = 0;
            }
            $this->cart_contents['cart_total'] += $this->cart_contents['delivery_fee'];

        }
        else{
            unset($this->cart_contents['delivery_fee']);
        }

    }

    public function applyPromotion($item,$rowId): void
    {

            foreach ($this->pizzasPromoted as $eachPromoted){
                if($item['id'] === $eachPromoted->getPizza()->getId()){
                    $discount = ($eachPromoted->getDiscountRate() *  $eachPromoted->getPizza()->getPrice() / 100) *  $item['qty'];
                    $this->cart_contents['cart_total'] = $this->cart_contents['cart_total'] - $discount;
                    $this->cart_contents[$rowId]['subtotal'] =  $this->cart_contents[$rowId]['subtotal']  - $discount;
                    $this->cart_contents[$rowId]['discount'] = $discount;
                }

            }
    }

}