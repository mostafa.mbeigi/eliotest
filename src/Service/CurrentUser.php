<?php

namespace App\Service;


use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class CurrentUser
{
    private Security $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function getCurrentUser(){
        if($this->security->getUser()){
            return $this->security->getUser();
        }
        return  null;
    }

}
