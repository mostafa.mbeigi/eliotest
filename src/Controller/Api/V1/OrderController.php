<?php


namespace App\Controller\Api\V1;

use App\Entity\Customer;
use App\Entity\Extras;
use App\Entity\Order;
use App\Entity\OrderItems;
use App\Entity\Pizzas;
use App\Form\CartType;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


class OrderController extends CartController
{
    private $groups = array('list',
        'customer'=> 'list',
        'orderItems'=> 'list',
        'pizzas'=>'read'
    );
    /*public function __construct(SerializerInterface $serializer, OrderRepository $orderRepository,EntityManagerInterface $em,SessionInterface $session){
        $this->orderRepository = $orderRepository;
        $this->serializer = $serializer;
        $this->em = $em;
        $this->session = $session;
    }*/
    /**
     * @Route("/orders",methods={"GET"},name="orders_list")
     * @param Request $request
     * @param OrderRepository $orderRepository
     * @return Response
     */
    public function getOrders(Request $request,OrderRepository $orderRepository){
        $filters = $request->query->all();
        $orders = $orderRepository->getOrders($filters);
        $responseData = ['status' => true, "data"=> $orders];
        return new Response(
            $this->serializer->serialize($responseData, 'json',["groups" => $this->groups]),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );

    }
    /**
     * @Route("/orders",methods={"POST"},name="create_order")
     * @param Request $request
     */
    public function store(Request $request,EntityManagerInterface $em){
        $form = $this->createForm(OrderType::class);
        $form->handleRequest($request);
        $parameters = json_decode($request->getContent(), true);
        $form->submit($parameters['data']);
        $formData = $parameters['data'];
        $cart_items = $this->contents();
        if (($form->isValid()) && (count($cart_items)>0)) {
            //create customer
            $customer = new Customer();
            $customer->setFirstname($formData['firstname']);
            $customer->setLastname($formData['firstname']);
            $customer->setPhone($formData['firstname']);
            $customer->setCity($formData['firstname']);
            $customer->setStreetnumber($formData['firstname']);
            $customer->setZip($formData['firstname']);
            $em->persist($customer);
            $pizzasExtrasIds = $this->getPizzasExtrasIds($cart_items );
            $pizzas = $em->getRepository(Pizzas::class)->getPizzasByIds($pizzasExtrasIds['pizzas']);
            $extras = $em->getRepository(Extras::class)->getExtrasByIds($pizzasExtrasIds['extras']);
            // create order
            $oder = new Order();
            $oder->setStatus("received");
            $oder->setCustomer($customer);
            $oder->setTotalPrice($this->cart_contents['cart_total']);
            //create order items
            foreach ($cart_items as $eachItem){
                $orderItems = new OrderItems();
                $orderItems->setQuantity($eachItem['qty']);
                foreach ($pizzas as $pizza){
                    if($pizza['id'] == $eachItem['id']){
                        $pizzaItem = $this->serializer->denormalize($pizza,Pizzas::class);
                        $orderItems->setPizzas($pizzaItem);
                    }

                }
                if(array_key_exists('extras', $eachItem)){
                    foreach ($eachItem['extras'] as $eachExtra){
                        foreach ($extras as $eachExtras) {
                            if ($eachExtra['id'] === $eachExtras['id']){
                                $extraItem = $this->serializer->denormalize($eachExtras, Extras::class);
                                $orderItems->addExtra($extraItem);
                            }

                        }
                    }

                }
                $orderItems->setSubtotal($eachItem['subtotal']);
                $orderItems->setCustomerOrder($oder);
                $em->persist($orderItems);
            }
            $em->flush();
            $responseData = ['status' => true, "data"=> array("message"=>"success")];
            return new Response(
                $this->serializer->serialize($responseData, 'json'),
                Response::HTTP_OK,
                ['Content-type' => 'application/json']
            );

        }
        $responseData = ['status' => false, "data"=> array("message"=>"Invalid data")];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_BAD_REQUEST,
            ['Content-type' => 'application/json']
        );

    }


    /**
     * @Route("/orders/{id}",methods={"PATCH"},name="edit_order")
     * @ParamConverter("order", class="App\Entity\Order")
     * @param Order $order
     * @param Request $request
     */
    public function update(Order $order,Request $request){
        $form = $this->createForm(OrderType::class);
        $form->handleRequest($request);
        $parameters = json_decode($request->getContent(), true);
        $form->submit($parameters['data']);
        $formData = $parameters['data'];
        if ($form->isValid()) {
            if (isset($formData['status'])) {
                $order->setStatus($formData['status']);
                $this->em->persist($order);
                $this->em->flush();
            }
            $responseData = ['status' => true, "data"=> array("message"=>"success")];
            return new Response(
                $this->serializer->serialize($responseData, 'json'),
                Response::HTTP_OK,
                ['Content-type' => 'application/json']
            );
        }
        $responseData = ['status' => false, "data"=> array("message"=>"Invalid data")];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_BAD_REQUEST,
            ['Content-type' => 'application/json']
        );
    }


    private function getPizzasExtrasIds($cartItems){
        $pizzasIds = [];
        $extrasIds = [];
        foreach ($cartItems as $cartItem){
            array_push($pizzasIds,$cartItem['id']);
            if(array_key_exists('extras', $cartItem)){
                foreach ($cartItem['extras'] as $extra){
                    array_push($extrasIds,$extra['id']);
                }
            }
        }
        return ['extras'=>$extrasIds,'pizzas'=>$pizzasIds];
    }

}