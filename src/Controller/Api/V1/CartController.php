<?php

namespace App\Controller\Api\V1;
use App\Classes\Shop;
use App\Entity\Pizzas;
use App\Entity\Setting;
use App\Form\CartType;
use App\Repository\ExtrasRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends Shop
{



    /**
     * @Route("/carts",methods={"GET"},name="cart_contents")
     * @return Response
     */
    public function index(){
        $responseData = ['status' => true, 'data'   => $this->cart_contents];
        return new Response(
            $this->serializer->serialize($responseData, 'json',["groups" => "list"]),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
    }

    /**
     * @Route("/carts",methods={"POST"},name="cart_add")
     * @param Request $request
     * @param ExtrasRepository $extrasRepository
     * @return Response
     */
    public function add(Request $request,ExtrasRepository $extrasRepository){
        $form = $this->createForm(CartType::class);
        $form->handleRequest($request);
        $parameters = json_decode($request->getContent(), true);
        $form->submit($parameters['data']);
        $formData = $parameters['data'];
        if ($form->isValid()) {
            $pizza = $this->em->getRepository(Pizzas::class)->find($formData["pizzas"]);
            $pizza = $this->serializer->normalize($pizza, null);
            $pizza["qty"] = $parameters['data']["qty"];
            if(array_key_exists('extras',$parameters['data'])){
                $pizza["extras"] = $extrasRepository->getExtrasByIds($formData['extras']);
            }
            $this->generateCartItem($pizza);
            // save Cart Item
            $this->save_cart();
            $responseData = ['status' => true, "data"=> $this->cart_contents];
            return new Response(
                $this->serializer->serialize($responseData, 'json'),
                Response::HTTP_OK,
                ['Content-type' => 'application/json']
            );
        }
        $responseData = ['status' => false, "data"=> array("message"=>"Invalid data")];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_BAD_REQUEST,
            ['Content-type' => 'application/json']
        );

    }
    /**
     * @Route("/carts",methods={"PUT"},name="cart_edit")
     * @param Request $request
     * @return Response
     */
    public function updateEntireCart(Request $request){
        $form = $this->createForm(CartType::class);
        $form->handleRequest($request);
        $parameters = json_decode($request->getContent(), true);
        $form->submit($parameters['data']);
        $formData = $parameters['data'];
        if ($form->isValid() && $this->cart_contents["total_items"]>0) {
            if (isset($formData['delivery'])) {
                $this->delivery = $formData['delivery'];
            }
            $this->save_cart();
            $responseData = ['status' => true, "data"=> $this->cart_contents];
            return new Response(
                $this->serializer->serialize($responseData, 'json',["groups" => "list"]),
                Response::HTTP_OK,
                ['Content-type' => 'application/json']
            );
        }
        $responseData = ['status' => false, "data"=> array("message"=>"Invalid data")];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_BAD_REQUEST,
            ['Content-type' => 'application/json']
        );
    }
    /**
     * @Route("/carts/{rowId}",methods={"PUT"},name="cart_item_edit")
     * @param $rowId
     * @param Request $request
     * @return Response
     */
    public function updateCartItem($rowId,Request $request){
        $form = $this->createForm(CartType::class);
        $form->handleRequest($request);
        $parameters = json_decode($request->getContent(), true);
        $form->submit($parameters['data']);
        $formData = $parameters['data'];
        if ($form->isValid() && (isset($this->cart_contents[$rowId]))) {
            if (isset($formData['qty'])) {
                $item['qty'] = (float)$formData['qty'];
                $this->changeQty($rowId,$formData['qty']);
            }
            $this->save_cart();
            $responseData = ['status' => true, "data"=> $this->cart_contents];
            return new Response(
                $this->serializer->serialize($responseData, 'json',["groups" => "list"]),
                Response::HTTP_OK,
                ['Content-type' => 'application/json']
            );
        }
        $responseData = ['status' => false, "data"=> array("message"=>"Invalid data")];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_BAD_REQUEST,
            ['Content-type' => 'application/json']
        );
    }
    /**
     * @Route("/carts/all",methods={"DELETE"},name="cart_delete_all")
     * @return Response
     */
    public function deleteAll():Response{
        $this->cart_contents = array('cart_total' => 0, 'total_items' => 0);
        try {
            $this->session->remove('cart_contents');
            $responseData = ['status' => true, "data"=>array("message"=>"success")];
            return new Response(
                $this->serializer->serialize($responseData, 'json'),
                Response::HTTP_OK,
                ['Content-type' => 'application/json']
            );
        }
        catch (\Exception $exception){
            $responseData = ['status' => false, "data"=> array("message"=>"unexpected error")];
            return new Response(
                $this->serializer->serialize($responseData, 'json'),
                Response::HTTP_BAD_REQUEST,
                ['Content-type' => 'application/json']
            );
        }

    }

    /**
     * @Route("/carts/{rowId}",methods={"DELETE"},name="cart_delete_pizza")
     * @param string $rowId
     * @return Response
     */
    public function deletePizzas(string $rowId):Response{
        if(isset($this->cart_contents[$rowId])){
            unset($this->cart_contents[$rowId]);
            $this->save_cart();
            $responseData = ['status' => true, "data"=> $this->cart_contents];
            return new Response(
                $this->serializer->serialize($responseData, 'json',["groups" => "list"]),
                Response::HTTP_OK,
                ['Content-type' => 'application/json']
            );
        }
        $responseData = ['status' => false, "data"=> array("message"=>"Invalid data")];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_BAD_REQUEST,
            ['Content-type' => 'application/json']
        );
    }

    /**
     * @Route("/carts/{rowId}/extras/{extraId}",methods={"DELETE"},name="cart_delete_extra")
     * @param string $rowId
     * @param int $extraId
     * @return Response
     */
    public function deleteExtras(string $rowId,int $extraId):Response{
        if((isset($this->cart_contents[$rowId]))&&(isset($this->cart_contents[$rowId]["extras"]))){
            foreach ($this->cart_contents[$rowId]["extras"] as $key=>$extra){
                if($extra['id'] == $extraId){
                    unset($this->cart_contents[$rowId]["extras"][$key]);
                    if(count($this->cart_contents[$rowId]["extras"])==0){
                        unset($this->cart_contents[$rowId]["extras"]);
                    }
                    $tempEditedItem = $this->cart_contents[$rowId];
                    unset($this->cart_contents[$rowId]);
                    $this->generateCartItem($tempEditedItem);
                    $this->save_cart();
                    $responseData = ['status' => true, "data"=> $this->cart_contents];
                    return new Response(
                        $this->serializer->serialize($responseData, 'json',["groups" => "list"]),
                        Response::HTTP_OK,
                        ['Content-type' => 'application/json']
                    );
                }
            }

        }
        $responseData = ['status' => false, "data"=> array("message"=>"Invalid data")];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_BAD_REQUEST,
            ['Content-type' => 'application/json']
        );
    }
    private function changeQty(string $rowId,int $newQty):void{
        $this->cart_contents[$rowId]['qty'] = $newQty;
    }
    private function generateCartItem(array $item):void{
        $newRowId = $this->generateRowId($item);
        if(isset($this->cart_contents[$newRowId])){
            $this->cart_contents[$newRowId]["qty"] += $item["qty"];
            return;
        }
        $this->cart_contents[$newRowId] = $item;
        $this->cart_contents[$newRowId]['qty'] = $item['qty'];
    }
    private function generateRowId(array $item):string{
        $rowId = $item['id'];
        if(array_key_exists('extras',$item)){
            $extraRowId = $this->generateAllExtrasIds($item['extras']);
            return md5($rowId.$extraRowId);
        }
        return md5($rowId);
    }
    protected function generateAllExtrasIds(array $extras):string {
        $tempArray = [];
        foreach ($extras as $extra){
            array_push($tempArray,$extra['id']);
        }
        return implode(',',$tempArray);
    }
    public function save_cart():void{
        $this->cart_contents['total_items'] = $this->cart_contents['cart_total'] = 0;
        foreach ($this->cart_contents as $key => $val){
            // make sure the array contains the proper indexes
            if(!is_array($val) OR !isset($val['price'], $val['qty'])){
                continue;
            }
            $extra_total = 0;
            if(array_key_exists("extras",$val)){
                foreach ($val["extras"] as $extra){
                    $extra_total += (float) $extra["price"] * $val['qty'];
                }
            }
            $this->cart_contents['cart_total'] += ($val['price'] * $val['qty'] + $extra_total);
            $this->cart_contents['total_items'] += $val['qty'];
            $this->cart_contents[$key]['subtotal'] = ($this->cart_contents[$key]['price'] * $this->cart_contents[$key]['qty'] + $extra_total);
            $this->applyPromotion($val,$key);
        }
        $this->cart_contents['cart_subtotal'] = $this->cart_contents['cart_total'] ;
        // manipulate cart based on delivery
        $this->calculate_delivery_fee();
        // if cart empty, delete it from the session
        if(count($this->cart_contents) <= 2){
            $this->destroy();
        }else{
            $this->session->set('cart_contents',$this->cart_contents);
        }

    }
    public function total_items(){
        return $this->cart_contents['total_items'];
    }
    public function destroy(){
        $this->cart_contents = array('cart_total' => 0, 'total_items' => 0);
        $this->session->remove('cart_contents');
    }
    protected function contents(){
        $cart = array_reverse($this->cart_contents);
        if(array_key_exists('delivery_fee', $cart)){
            unset($cart['delivery_fee']);
        }
        unset($cart['total_items']);
        unset($cart['cart_subtotal']);
        unset($cart['cart_total']);
        return $cart;
    }

}