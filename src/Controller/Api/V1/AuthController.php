<?php


namespace App\Controller\Api\V1;


use App\Entity\User;
use App\Service\CurrentUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Security;
class AuthController extends AbstractController
{
    private SerializerInterface $serializer;
    private $security;
    public function __construct(SerializerInterface $serializer,Security $security)
    {
        $this->serializer = $serializer;
        $this->security = $security;
    }

    /**
     * @Route(path="/users/me" )
     * @IsGranted("IS_AUTHENTICATED_ANONYMOUSLY")
     * @param CurrentUser $currentUser
     * @return Response
     */
    public function show(CurrentUser $currentUser){
           if($currentUser->getCurrentUser()){
               $responseData = ['status' => true, "data"=> $currentUser->getCurrentUser()];
               return new Response(
                   $this->serializer->serialize($responseData, 'json',["groups"=>"list"]),
                   Response::HTTP_OK,
                   ['Content-type' => 'application/json']
               );
           }
        $responseData = ['status' => false, "data"=> array("message"=>"not Authorized")];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );

    }
}