<?php


namespace App\Controller\Api\V1;


use App\Classes\Shop;
use App\Repository\PromotionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PromotionController extends Shop
{
    /**
     * @Route("/promotions",methods={"GET"},name="promotions")
     * @param PromotionRepository $promotionRepository
     * @return Response
     */
    public function index(PromotionRepository $promotionRepository){
        $promotions = $promotionRepository->findAll();
        $responseData = ['status' => true, "data"=> $promotions];
        return new Response(
            $this->serializer->serialize($responseData, 'json'),
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
    }


}