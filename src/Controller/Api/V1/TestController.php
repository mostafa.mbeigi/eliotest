<?php


namespace App\Controller\Api\V1;


use App\Entity\Navigation;
use App\Entity\NavigationTranslation;

use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use Knp\DoctrineBehaviors\DoctrineBehaviorsBundle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerBuilder;
class TestController extends AbstractController
{
    /**
     * @Route(path="/test",methods={"GET"})
     * @param EntityManagerInterface $entityManager
     */
   public function index(EntityManagerInterface $entityManager,Request $request){

       /*$locale = $request->getLocale();
       dd($locale);*/
       $category = new Navigation();
       $category->translate('de')->setTitle('Über uns');
       $category->translate('en')->setTitle('About');
       $entityManager->persist($category);
       $category->mergeNewTranslations();

       $category2 = new Navigation();
       $category2->translate('de')->setTitle('imprint');
       $category2->translate('en')->setTitle('imprint');
       $entityManager->persist($category2);
       $category2->mergeNewTranslations();

       $category3 = new Navigation();
       $category3->translate('de')->setTitle('Kontakt');
       $category3->translate('en')->setTitle('contact');
       $entityManager->persist($category3);
       $category3->mergeNewTranslations();

       $category4 = new Navigation();
       $category4->translate('de')->setTitle('Online order');
       $category4->translate('en')->setTitle('Online Bestellung');
       $entityManager->persist($category4);
       $category4->mergeNewTranslations();


       $entityManager->flush();


       $all = $entityManager->getRepository(Navigation::class)->findAll();
       $serializer = SerializerBuilder::create()->build();
      $json=$serializer->serialize($all, 'json', SerializationContext::create()->setGroups(array('list')));
       return new JsonResponse($json, 200, [], true);



       //return $serializer->serialize($all,'json');
   }
}