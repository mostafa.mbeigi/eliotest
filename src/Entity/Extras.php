<?php

namespace App\Entity;

use App\Repository\ExtrasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;


#[ORM\Entity(repositoryClass: ExtrasRepository::class)]
#[ApiResource(
normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
)]
class Extras
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["read"])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(["read", "write"])]
    private ?string $name = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    #[Groups(["read", "write"])]
    private ?string $price = null;

    #[ORM\Column]
    #[Groups(["read", "write"])]
    private ?bool $isChoosable = null;

    #[ORM\ManyToMany(targetEntity: Pizzas::class, mappedBy: 'extras')]
    private Collection $pizzas;

    #[ORM\ManyToMany(targetEntity: OrderItems::class, mappedBy: 'extras' )]
    private Collection $orderItems;


    public function __construct()
    {
        $this->pizzas = new ArrayCollection();
        $this->orderItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function isIsChoosable(): ?bool
    {
        return $this->isChoosable;
    }

    public function setIsChoosable(bool $isChoosable): self
    {
        $this->isChoosable = $isChoosable;

        return $this;
    }

    /**
     * @return Collection<int, Pizzas>
     */
    public function getPizzas(): Collection
    {
        return $this->pizzas;
    }

    public function addPizza(Pizzas $pizza): self
    {
        if (!$this->pizzas->contains($pizza)) {
            $this->pizzas->add($pizza);
            $pizza->addExtra($this);
        }

        return $this;
    }

    public function removePizza(Pizzas $pizza): self
    {
        if ($this->pizzas->removeElement($pizza)) {
            $pizza->removeExtra($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, OrderItems>
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItems $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems->add($orderItem);
            $orderItem->addExtra($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItems $orderItem): self
    {
        if ($this->orderItems->removeElement($orderItem)) {
            $orderItem->removeExtra($this);
        }

        return $this;
    }


}
