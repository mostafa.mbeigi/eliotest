<?php

namespace App\Entity;

use App\Repository\OrderItemsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;



#[ORM\Entity(repositoryClass: OrderItemsRepository::class)]
class OrderItems
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(["list"])]
    private ?int $id = null;

    #[ORM\Column]
    #[Groups(["list"])]
    private ?int $quantity = null;


    #[ORM\ManyToMany(targetEntity: Extras::class, inversedBy: 'orderItems' ,cascade:["persist"] )]
    #[Groups(["list"])]
    private Collection $extras;

    #[ORM\ManyToOne(inversedBy: 'orderItems',cascade:["persist"] )]
    #[Groups(["list"])]
    private ?Pizzas $pizzas = null;

    #[ORM\ManyToOne(inversedBy: 'orderItems',cascade:["persist"])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Order $customerOrder = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private ?string $subtotal = null;

    public function __construct()
    {
        $this->extras = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }


    /**
     * @return Collection<int, Extras>
     */
    public function getExtras(): Collection
    {
        return $this->extras;
    }

    public function addExtra(Extras $extra): self
    {
        if (!$this->extras->contains($extra)) {
            $this->extras->add($extra);
        }

        return $this;
    }

    public function removeExtra(Extras $extra): self
    {
        $this->extras->removeElement($extra);

        return $this;
    }

    public function getPizzas(): ?Pizzas
    {
        return $this->pizzas;
    }

    public function setPizzas(?Pizzas $pizzas): self
    {
        $this->pizzas = $pizzas;

        return $this;
    }

    public function getCustomerOrder(): ?Order
    {
        return $this->customerOrder;
    }

    public function setCustomerOrder(?Order $customerOrder): self
    {
        $this->customerOrder = $customerOrder;

        return $this;
    }

    public function getSubtotal(): ?string
    {
        return $this->subtotal;
    }

    public function setSubtotal(string $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }
}
