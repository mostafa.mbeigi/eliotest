<?php

namespace App\Form;

use App\Constants\TimeEvaluationConstants;
use App\Entity\Extras;
use App\Entity\Pizzas;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('zip')
            ->add('phone')
            ->add('street')
            ->add('streetnumber')
            ->add('city')
            ->add('qty')
            ->add('status',ChoiceType::class,[
                'choices'=>[
                    "received"=> "received",
                    "processed"=>"processed",
                ]]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection'=>false
            // Configure your form options here
        ]);
    }
}
