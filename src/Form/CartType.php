<?php

namespace App\Form;

use App\Entity\Extras;
use App\Entity\Pizzas;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CartType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pizzas',EntityType::class,[
                'class' => Pizzas::class,
                'choice_label' => 'id',
            ])
            ->add('extras',EntityType::class,[
                'class' => Extras::class,
                'expanded'  => true,
                'multiple'  => true,
                'choice_label'=>'id',
            ])
            ->add('delivery',ChoiceType::class,[
                'choices'=>[
                    1=>1,
                    0=>0
                ],
                'empty_data'=>"0"
            ])
            ->add('qty',NumberType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'csrf_protection'=>false
        ]);
    }
}
